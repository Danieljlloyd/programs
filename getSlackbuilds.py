import os
import subprocess

slackwareVersion = '14.2'

# Put each slackware version's sources in it's own directory
sourceDir = os.environ['HOME'] + '/src/' + slackwareVersion + '/'
if not os.path.isdir(sourceDir):
    subprocess.check_call(['mkdir', sourceDir])

# List of slackbuilds
slackbuilds = [
        { 'name': 'libwebp', 'category': 'libraries' },
        { 'name': 'webkitgtk', 'category': 'libraries' },
        { 'name': 'json-c', 'category': 'libraries' },
        { 'name': 'dwb', 'category': 'network' },
        { 'name': 'girara', 'category': 'libraries' },
        { 'name': 'zathura', 'category': 'office' }
]

# For each slackbuild, create a directory and download the slackbuild sources
for build in slackbuilds:
    dirName = sourceDir + build['name'] + '/'
    if not os.path.isdir(dirName):
        try:
            # Create a directory for the new slackbuild
            subprocess.check_call(['mkdir', dirName])
    
            # Scrape the webpage to find the upstream source and the slackbuild script
            webpageUrl = 'https://slackbuilds.org/repository/{}/{}/{}/'.format(slackwareVersion, build['category'], build['name'])
            webpageFile = dirName + 'slackbuildPage'
            subprocess.check_call(['wget', webpageUrl, '-O', webpageFile]) 
            pipe = subprocess.check_output(['cat', webpageFile]) 

            for line in pipe.split('\n'):
                if 'Source Downloads:' in line:
                    upstreamUrl = line.split('"')[1]

            for line in pipe.split('\n'):
                if 'Download SlackBuild:' in line:
                    slackbuildUrl = 'https://slackbuilds.org' + line.split('"')[1]

            subprocess.check_call(['rm', webpageFile])

            # Download the upstream source and slackbuild
            slackbuildArchive = dirName + slackbuildUrl.split('/')[-1]
            upstreamArchive = dirName + build['name'] + '/' + upstreamUrl.split('/')[-1]

            subprocess.check_call(['wget', slackbuildUrl, '-O', slackbuildArchive])
            subprocess.check_call(['tar', 'zxvf', slackbuildArchive, '-C', dirName])

            subprocess.check_call(['wget', upstreamUrl, '-O', upstreamArchive])
        except Exception as e:
            print 'Couldnt finish slackbuild for {} -- {}'.format(build['name'], str(e))
            subprocess.check_call(['rm', '-rf', dirName])        
    else:
            print 'Already have sources for {}'.format(build['name'])
