import os
import subprocess

sourceDir = os.environ['HOME'] + '/src/repos'

# List of git repos 
repos = [ 
]

# For each repo clone into src/{name}
for repo in repos:
    try:
        dirName = sourceDir + repo['name'] 
        if not os.path.isdir(dirName):
            subprocess.check_call(['git', 'clone', repo['url'], dirName])
        else:
            print 'Already have sources for {}'.format(repo['name'])
    
    except Exception as e:
        print 'Error downloading source for {} -- {}'.format(repo['name'], str(e))
